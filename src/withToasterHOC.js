import React from 'react';
import { useToasts } from 'react-toast-notifications';

export const withToasterHOC = (Component) => {
  return (props) => {
    const { addToast } = useToasts();

    return (
      <Component addToast={addToast} {...props} />
    );
  };
};
