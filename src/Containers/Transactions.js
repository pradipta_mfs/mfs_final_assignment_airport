import React from 'react';
import { connect } from "react-redux";
import store from "../Store/TransactionStore"
import airportStore from "../Store/AirportStore"
import aircraftStore from "../Store/AircraftStore"
import { addTransaction, deleteTransaction, reverseTransaction } from "../Actions/TransactionAction";
import { Button, Form, Row, Col, Table } from 'react-bootstrap';
import { withToasterHOC } from '../withToasterHOC';
import moment from "moment";

const mapStateToProps = state => {
  return { transactions: state.transactions.sort((prev, next) => (new Date(next.transaction_date_time) - new Date(prev.transaction_date_time))) };
};

function mapDispatchToProps(dispatch) {
  return {
    addTransaction: transaction => dispatch(addTransaction(transaction)),
    deleteTransaction: transaction_id => dispatch(deleteTransaction(transaction_id)),
    reverseTransaction: transaction => dispatch(reverseTransaction(transaction))
  };
}

class Transactions extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      transaction_date_time: '',
      transaction_type: '',
      airport_id: '',
      aircraft_id: '',
      quantity: '',
      transactions: props.transactions,
      error: {
        transaction_type: '',
        airport_id: '',
        aircraft_id: '',
        quantity: ''
      }
    };
    this.airports = airportStore.getState().airports;
    this.aircrafts = aircraftStore.getState().aircrafts;
  }

  handleChange(event) {
    const { id, value } = event.target;
    const error = Object.assign({}, this.state.error);
    switch(id) {
      case 'transaction_type':
      case 'airport_id':
      case 'aircraft_id':
        error[id] = value === '' ? 'Please choose a valid value' : ''
        break
      case 'quantity':
        error[id] = value <= 0 ? 'Quantity must be greater than 0' : ''
        break
      default:
        break
    }
    this.setState({
      [id]: value,
      error: error
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    const { transaction_date_time, transaction_type, airport_id, aircraft_id, quantity } = this.state;
    if(!transaction_date_time || !transaction_type || !airport_id || !aircraft_id || !quantity) {
      return;
    }
    if(transaction_type === 'OUT' && (this.airports.find(airport => airport.airport_id === parseInt(airport_id))).fuel_available < quantity) {
      this.props.addToast('Airport does not have enough fuel available', { appearance: 'error' });
      return;
    }
    if(!moment(transaction_date_time, 'DD/MM/YYYY HH:mm', true).isValid()) {
      this.props.addToast('Please enter a valid date', { appearance: 'error' });
      return;
    }
    this.props.addTransaction(Object.assign({ reversed: false }, {
      transaction_date_time,
      transaction_type,
      airport_id: parseInt(airport_id),
      aircraft_id: parseInt(aircraft_id),
      quantity: parseInt(quantity)
    }));
    this.setState(
      {
        transaction_date_time: "",
        transaction_type: "",
        airport_id: "",
        aircraft_id: "",
        quantity: "",
        transactions: store.getState().transactions
      });
  }

  deleteHandler(transaction_id) {
    this.props.deleteTransaction(transaction_id);
    this.setState({
      transactions: store.getState().transactions
    });
  }

  reverseHandler(transaction_id) {
    const { transaction_date_time, transaction_type, airport_id, aircraft_id, quantity } = this.state.transactions.find(trans=> trans.transaction_id === transaction_id);
    if(transaction_type === 'IN' && (this.airports.find(airport => airport.airport_id === parseInt(airport_id))).fuel_available < quantity) {
      this.props.addToast('Airport does not have enough fuel available', { appearance: 'error' });
      return;
    }
    this.props.reverseTransaction({
      transaction_date_time,
      transaction_type: transaction_type === 'IN' ? 'OUT': 'IN',
      airport_id,
      aircraft_id,
      quantity,
      transaction_id_parent: transaction_id,
      reversed: true
    });
    this.setState({
      transactions: store.getState().transactions
    });
  }

  render() {
    const { transaction_date_time, transaction_type, airport_id, aircraft_id, quantity } = this.state;
    return (
      <React.Fragment>
        <Form onSubmit={(e) => this.handleSubmit(e)} style={{'padding': '80px 20px'}}>
          <Form.Group as={Row}>
            <Form.Label column sm={3}>
              Transaction Date:
            </Form.Label>
            <Col sm={{ span: 6 }}>
              <Form.Control
                type="text"
                placeholder="DD/MM/YYYY HH:MM"
                id="transaction_date_time"
                value={transaction_date_time}
                onChange={(e) => this.handleChange(e)}
              />
            </Col>
          </Form.Group>

          <Form.Group as={Row}>
            <Form.Label column sm={3}>
              Transaction Type:
            </Form.Label>
            <Col sm={{ span: 6 }}>
              <Form.Control as="select" id="transaction_type" value={transaction_type} onChange={(e) => this.handleChange(e)} custom>
                <option value=""></option>
                <option value="IN">IN</option>
                <option value="OUT">OUT</option>
              </Form.Control>
            </Col>
            <Col sm={3}>
              {this.state.error.transaction_type.length > 0 && <span style={{color: "red"}}>{this.state.error.transaction_type}</span>}
            </Col>
          </Form.Group>

          <Form.Group as={Row}>
            <Form.Label column sm={3}>
              Airport:
            </Form.Label>
            <Col sm={{ span: 6 }}>
              <Form.Control as="select" id="airport_id" value={airport_id} onChange={(e) => this.handleChange(e)} custom>
                <option value=""></option>
                {
                  this.airports.map(airport => {
                    return (
                      <option value={airport.airport_id} key={airport.airport_id}>{airport.airport_name}</option>
                    )
                  })
                }
              </Form.Control>
            </Col>
            <Col sm={3}>
              {this.state.error.airport_id.length > 0 && <span style={{color: "red"}}>{this.state.error.airport_id}</span>}
            </Col>
          </Form.Group>

          <Form.Group as={Row}>
            <Form.Label column sm={3}>
              Aircraft:
            </Form.Label>
            <Col sm={{ span: 6 }}>
              <Form.Control as="select" id="aircraft_id" value={aircraft_id} onChange={(e) => this.handleChange(e)} custom>
                <option value=""></option>
                {
                  this.aircrafts.map(aircraft => {
                    return (
                      <option value={aircraft.aircraft_id} key={aircraft.aircraft_id}>{aircraft.aircraft_no}</option>
                    )
                  })
                }
              </Form.Control>
            </Col>
            <Col sm={3}>
              {this.state.error.aircraft_id.length > 0 && <span style={{color: "red"}}>{this.state.error.aircraft_id}</span>}
            </Col>
          </Form.Group>

          <Form.Group as={Row}>
            <Form.Label column sm={3}>
              Fuel Quantity:
            </Form.Label>
            <Col sm={{ span: 6 }}>
              <Form.Control
                type="number"
                placeholder="Fuel Quantity"
                id="quantity"
                value={quantity}
                onChange={(e) => this.handleChange(e)}
              />
            </Col>
          </Form.Group>
          <Form.Group as={Row}>
            <Col sm={{ span: 10 }}>
              <Button type="submit">SAVE</Button>
            </Col>
          </Form.Group>
        </Form>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              <th>Transaction Date</th>
              <th>Type</th>
              <th>Airport</th>
              <th>Aircraft</th>
              <th>Quantity</th>
              <th>Parent Transaction</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
          {
            this.state.transactions.map((item) => {
              return (
                <tr key ={item.transaction_id.toString()}>
                  <td>{item.transaction_id}</td>
                  <td>{item.transaction_date_time}</td>
                  <td>{item.transaction_type}</td>
                  <td>{(this.airports.find(el => el.airport_id === item.airport_id)).airport_name}</td>
                  <td>{(this.aircrafts.find(el => el.aircraft_id === item.aircraft_id)).aircraft_no}</td>
                  <td>{item.quantity}</td>
                  <td>{item.transaction_id_parent}</td>
                  <td>
                    <Button variant="link" onClick={() => this.deleteHandler(item.transaction_id)}>Delete</Button>
                    {!item.reversed && <Button variant="link" onClick={() => this.reverseHandler(item.transaction_id)}>Reverse</Button>}
                  </td>
                </tr>
              )
            })
          }
          </tbody>
        </Table>
      </React.Fragment>
    )
  }
}

const Transaction = connect(mapStateToProps, mapDispatchToProps)(Transactions);

export default withToasterHOC(Transaction);
