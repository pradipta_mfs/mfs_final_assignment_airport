import React from "react";
import AirportStore from "../Store/AirportStore";
import AircraftStore from "../Store/AircraftStore";
import TransactionStore from "../Store/TransactionStore";
import { calculateAvailableFuel } from "../Actions/AirportAction"

class Reports extends React.Component {
  constructor() {
    super();
    this.state = {
      summaryReport : [],
      consumptionReport: []
    }
    this.transactions = TransactionStore.getState().transactions;
    AirportStore.dispatch(calculateAvailableFuel(this.transactions));
    this.airports = AirportStore.getState().airports;
    this.aircrafts = AircraftStore.getState().aircrafts;
  }

  componentDidMount() {
    const summaryReport = [];
    const consumptionReport = [];
    this.airports.forEach(airport => {
      summaryReport.push({
        id: airport.airport_id,
        airport: airport.airport_name,
        fuel_available: airport.fuel_available
      });
      consumptionReport.push({
        id: airport.airport_id,
        airport: airport.airport_name,
        consumptions: this.transactions.filter(trans => trans.airport_id === airport.airport_id),
        fuel_available: airport.fuel_available
      });
    });
    this.setState({
      summaryReport: summaryReport,
      consumptionReport: consumptionReport
    });
  }

  render() {
    return (
      <div>
        <h1 style={{'borderBottom': '1px dotted'}}>Reports</h1>
        <h2>Airport Summary Report</h2>
        <table>
          <thead>
            <tr>
              <th>Airport</th>
              <th></th>
              <th>Fuel Available</th>
            </tr>
          </thead>
          <tbody>
            {this.state.summaryReport.map(summary => (
              <tr key={summary.id}>
                <td>{summary.airport}</td>
                <td></td>
                <td>{summary.fuel_available}</td>
              </tr>
            ))}
          </tbody>
        </table>
        <br />
        <h2>Fuel Consumption Report</h2>
        {
          this.state.consumptionReport.map(cons => (
            <React.Fragment key={cons.id}>
              <p>Airport: {cons.airport}</p>
              {
                cons.consumptions.map(consumption => (
                  <table style={{ borderBottom: '1px dotted', borderTop: '0.5px dotted' }} key={consumption.transaction_id}>
                    <thead>
                      <tr>
                        <th>Date/Time</th>
                        <th>Type</th>
                        <th>Fuel</th>
                        <th>Aircraft</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>{consumption.transaction_date_time}</td>
                        <td>{consumption.transaction_type}</td>
                        <td>{consumption.quantity}</td>
                        <td>{this.aircrafts.find(ac => ac.aircraft_id === consumption.aircraft_id).aircraft_no}</td>
                      </tr>
                    </tbody>
                  </table>
                ))
              }
              <br />
              <p>Fuel Available: {cons.fuel_available}</p>
              ---
            </React.Fragment>
          ))
        }
      </div>
    );
  }
}

export default Reports;
