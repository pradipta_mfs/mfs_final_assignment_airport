import React from 'react';
import { connect } from "react-redux";
import store from "../Store/AircraftStore"
import { addAircraft, deleteAircraft } from "../Actions/AircraftAction";
import { Button, Form, Row, Col, Table } from 'react-bootstrap';
import { withToasterHOC } from '../withToasterHOC';

const mapStateToProps = state => {
  return { aircrafts: state.aircrafts.sort((prev, next) => (prev.aircraft_no.localeCompare(next.aircraft_no))) };
};

function mapDispatchToProps(dispatch) {
  return {
    addAircraft: aircraft => dispatch(addAircraft(aircraft)),
    deleteAircraft: aircraft_id => dispatch(deleteAircraft(aircraft_id))
  };
}

class Aircrafts extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      aircraft_no: "",
      airline: "",
      error: {
        aircraft_no: '',
        airline: ''
      },
      disabled: true,
      aircrafts: props.aircrafts
    };
  }

  handleChange(event) {
    const { id, value } = event.target;
    const error = Object.assign({}, this.state.error);
    switch(id) {
      case 'aircraft_no':
      case 'airline':
        error[id] = value === '' ? 'Value can not be empty' : ''
        break
      default:
        break
    }
    this.setState({
      [id]: value,
      error: error
    });
    this.setState({ [event.target.id]: event.target.value });
  }

  handleSubmit(event) {
    event.preventDefault();
    const { aircraft_no, airline } = this.state;
    if(!aircraft_no || !airline) {
      return;
    }
    if(this.state.aircrafts.find(aircraft => aircraft.aircraft_no === aircraft_no.trim() && aircraft.airline === airline.trim())) {
      this.props.addToast('Aircraft already exist', { appearance: 'error' });
      return;
    }
    this.props.addAircraft({ aircraft_no: aircraft_no.trim(), airline: airline.trim() });
    console.log(store.getState());
    this.setState({ aircraft_no: "", airline: "", aircrafts: store.getState().aircrafts });
  }

  deleteHandler(aircraft_id) {
    this.props.deleteAircraft(aircraft_id);
    this.setState({
      aircrafts: store.getState().aircrafts
    });
  }

  render() {
    const { aircraft_no, airline } = this.state;
    return (
      <React.Fragment>
        <Form onSubmit={(e) => this.handleSubmit(e)} style={{'padding': '80px 20px'}}>
          <Form.Group as={Row}>
            <Form.Label column sm={3}>
              Aircraft No:
            </Form.Label>
            <Col sm={{ span: 6 }}>
              <Form.Control
                type="text"
                placeholder="Type Aircraft No"
                id="aircraft_no"
                value={aircraft_no}
                onChange={(e) => this.handleChange(e)}
              />
            </Col>
            <Col sm={3}>
              {this.state.error.aircraft_no.length > 0 && <span style={{color: "red"}}>{this.state.error.aircraft_no}</span>}
            </Col>
          </Form.Group>

          <Form.Group as={Row}>
            <Form.Label column sm={3}>
              Airline:
            </Form.Label>
            <Col sm={{ span: 6 }}>
              <Form.Control
                type="text"
                placeholder="Airline"
                id="airline"
                value={airline}
                onChange={(e) => this.handleChange(e)}
              />
            </Col>
            <Col sm={3}>
              {this.state.error.airline.length > 0 && <span style={{color: "red"}}>{this.state.error.airline}</span>}
            </Col>
          </Form.Group>
          <Form.Group as={Row}>
            <Col sm={{ span: 10 }}>
              <Button type="submit" disabled={this.state.disabled}>SAVE</Button>
            </Col>
          </Form.Group>
        </Form>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              <th>Aircraft No</th>
              <th>Airline</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
          {
            this.state.aircrafts.map((item) => {
              return (
                <tr key ={item.aircraft_id.toString()}>
                  <td>{item.aircraft_id}</td>
                  <td>{item.aircraft_no}</td>
                  <td>{item.airline}</td>
                  <td>
                    <Button variant="link" onClick={() => this.deleteHandler(item.aircraft_id)}>Delete</Button>
                  </td>
                </tr>
              )
            })
          }
          </tbody>
        </Table>
      </React.Fragment>
    )
  }
}

const Aircraft = connect(mapStateToProps, mapDispatchToProps)(Aircrafts);

export default withToasterHOC(Aircraft);
