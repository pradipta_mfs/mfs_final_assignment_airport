import React from 'react';
import { connect } from "react-redux";
import store from "../Store/AirportStore"
import transactionStore from "../Store/TransactionStore"
import { addAirport, calculateAvailableFuel, deleteAirport } from "../Actions/AirportAction";
import { Button, Form, Row, Col, Table } from 'react-bootstrap';
import { withToasterHOC } from '../withToasterHOC';

const mapStateToProps = state => {
  return { airports: state.airports.sort((prev, next) => (prev.airport_name.localeCompare(next.airport_name))) };
};

function mapDispatchToProps(dispatch) {
  return {
    addAirport: airport => dispatch(addAirport(airport)),
    calculateAvailableFuel: transactions => dispatch(calculateAvailableFuel(transactions)),
    deleteAirport: airport_id => dispatch(deleteAirport(airport_id))
  };
}

class Airports extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      airport_name: '',
      fuel_capacity: '',
      error: {
        airport_name: '',
        fuel_capacity: ''
      },
      airports: props.airports
    };
  }

  handleChange(event) {
    const { id, value } = event.target;
    const error = Object.assign({}, this.state.error);
    switch(id) {
      case 'airport_name':
        error[id] = value === '' ? 'Airport name can not be empty' : ''
        break
      case 'fuel_capacity':
        error[id] = value <= 0 ? 'Fuel Capacity must be greater than 0' : ''
        break
      default:
        break
    }
    this.setState({
      [id]: value,
      error: error
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    const { airport_name, fuel_capacity } = this.state;
    if(!airport_name || !fuel_capacity) {
      return;
    }
    if(this.state.airports.find(airport => airport_name.trim() === airport.airport_name)) {
      this.props.addToast('Airport already exist', { appearance: 'error' });
      return;
    }
    this.props.addAirport({ airport_name: airport_name.trim(), fuel_capacity, fuel_available: fuel_capacity });
    this.setState({ airport_name: '', fuel_capacity: '', airports: store.getState().airports });
  }

  deleteHandler(airport_id) {
    this.props.deleteAirport(airport_id);
    this.setState({
      airports: store.getState().airports
    });
  }

  componentDidMount() {
    const transactions = [...transactionStore.getState().transactions];
    this.props.calculateAvailableFuel(transactions);
    this.setState({
      airports: store.getState().airports
    });
  }

  render() {
    const { airport_name, fuel_capacity } = this.state;
    return (
      <React.Fragment>
        <Form onSubmit={(e) => this.handleSubmit(e)} style={{'padding': '80px 20px'}}>
          <Form.Group as={Row}>
            <Form.Label column sm={3}>
              Airport Name:
            </Form.Label>
            <Col sm={{ span: 6 }}>
              <Form.Control
                type="text"
                placeholder="Type Airport Name"
                id="airport_name"
                value={airport_name}
                onChange={(e) => this.handleChange(e)}
              />
            </Col>
            <Col sm={3}>
              {this.state.error.airport_name.length > 0 && <span style={{color: "red"}}>{this.state.error.airport_name}</span>}
            </Col>
          </Form.Group>

          <Form.Group as={Row}>
            <Form.Label column sm={3}>
              Fuel Capacity:
            </Form.Label>
            <Col sm={{ span: 6 }}>
              <Form.Control
                type="number"
                placeholder="Fuel Capacity"
                id="fuel_capacity"
                value={fuel_capacity}
                onChange={(e) => this.handleChange(e)}
              />
            </Col>
            <Col sm={3}>
              {this.state.error.fuel_capacity.length > 0 && <span style={{color: "red"}}>{this.state.error.fuel_capacity}</span>}
            </Col>
          </Form.Group>
          <Form.Group as={Row}>
            <Col sm={{ span: 10 }}>
              <Button type="submit">SAVE</Button>
            </Col>
          </Form.Group>
        </Form>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              <th>Airport Name</th>
              <th>Fuel Capacity</th>
              <th>Available Fuel</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
          {
            this.state.airports.map((item) => {
              return (
                <tr key ={item.airport_id.toString()}>
                  <td>{item.airport_id}</td>
                  <td>{item.airport_name}</td>
                  <td>{item.fuel_capacity}</td>
                  <td>{item.fuel_available}</td>
                  <td>
                    <Button variant="link" onClick={() => this.deleteHandler(item.airport_id)}>Delete</Button>
                  </td>
                </tr>
              )
            })
          }
          </tbody>
        </Table>
      </React.Fragment>
    )
  }
}

const Airport = connect(mapStateToProps, mapDispatchToProps)(Airports);

export default withToasterHOC(Airport);
