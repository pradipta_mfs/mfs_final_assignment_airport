import React, { useState } from "react";
import { Form, Button, FormGroup, FormControl } from "react-bootstrap";
import "./../Style/Login.css";
import { useAppContext } from "../libs/contextLib";
import { useHistory, Redirect } from "react-router-dom";

export default function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const { isAuthenticated, userHasAuthenticated } = useAppContext();
  const history = useHistory();

  function validateForm() {
    return email.length > 0 && password.length > 0;
  }

  function handleSubmit(event) {
    event.preventDefault();
    if(email === 'pdey@mindfiresolutions.com' && password === 'admin') {
      userHasAuthenticated(true);
      localStorage.setItem('user', JSON.stringify({ isLoggedIn: true, email: email }));
      history.push("/");
    } else {
      alert('Wrong Username and Password');
    }
  }

  return (
    !isAuthenticated?
    <div className="Login">
      <form onSubmit={handleSubmit}>
        <FormGroup controlId="email" size="lg">
          <Form.Label>Email</Form.Label>
          <FormControl
            autoFocus
            type="email"
            value={email}
            onChange={e => setEmail(e.target.value)}
          />
        </FormGroup>
        <FormGroup controlId="password" size="lg">
          <Form.Label>Password</Form.Label>
          <FormControl
            value={password}
            onChange={e => setPassword(e.target.value)}
            type="password"
          />
        </FormGroup>
        <Button block size="lg" disabled={!validateForm()} type="submit">
          Login
        </Button>
      </form>
    </div>:
    <Redirect to="/" />
  );
}
