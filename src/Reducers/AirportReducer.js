import { ADD_AIRPORT, CALCULATE_Transaction, DELETE } from "../Constants/actionTypes";
let id = 4;

const initialState = {
  airports: [{
    airport_id: 1,
    airport_name: 'Netaji Subhas Chandra Airport(CCU)',
    fuel_capacity: 50000,
    fuel_available: 50000
  }, {
    airport_id: 2,
    airport_name: 'Hindustan Airport (BLR)',
    fuel_capacity: 40000,
    fuel_available: 40000
  }, {
    airport_id: 3,
    airport_name: 'Chhatrapati Shivaji Airport (BOM)',
    fuel_capacity: 30000,
    fuel_available: 30000
  }, {
    airport_id: 4,
    airport_name: 'Bhubaneswar Airport (BBI)',
    fuel_capacity: 20000,
    fuel_available: 20000
  }]
};

function AirportReducer(state = initialState, action) {
  if (action.type === ADD_AIRPORT) {
    return Object.assign({}, state, {
      airports: state.airports.concat(Object.assign({ airport_id: ++id }, action.payload))
    });
  } else if(action.type === CALCULATE_Transaction) {
    const airports = [...state.airports];
    const transactions = [...action.payload]
    airports.forEach((airport) => {
      if (transactions.find(trans => trans.airport_id === airport.airport_id)) {
        const inValues = (transactions.filter(trans => trans.airport_id === airport.airport_id && trans.transaction_type === 'IN'));
        const outValues = (transactions.filter(trans => trans.airport_id === airport.airport_id && trans.transaction_type === 'OUT'));
        let inTotal = 0;
        let outTotal = 0;
        if (inValues.length > 0) {
          inValues.length === 1 ? inTotal = inValues[0].quantity :
          inTotal = inValues.reduce((prev, acc) => prev.quantity + acc.quantity);
        }
        if (outValues.length > 0) {
          outValues.length === 1 ? outTotal = outValues[0].quantity :
          outTotal = outValues.reduce((prev, acc) => prev.quantity + acc.quantity);
        }
        airport.fuel_available = (airport.fuel_capacity + inTotal) - outTotal;
      }
    })
    return Object.assign({}, state, {
      airports: airports
    })
  } else if(action.type === DELETE) {
    return Object.assign({}, state, {
      airports: state.airports.filter(el => el.airport_id !== action.payload)
    });
  }
  return state;
};

export default AirportReducer;
