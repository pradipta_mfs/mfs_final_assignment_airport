import { ADD_Transaction, REVERSE_Transaction, DELETE } from "../Constants/actionTypes";
let id = 0;

const initialState = {
  transactions: []
};

function TransactionReducer(state = initialState, action) {
  if (action.type === ADD_Transaction) {
    return Object.assign({}, state, {
      transactions: state.transactions.concat(Object.assign({ transaction_id: ++id }, action.payload))
    });
  } else if(action.type === DELETE) {
    return Object.assign({}, state, {
      transactions: state.transactions.filter(el => el.transaction_id !== action.payload)
    });
  } else if(action.type === REVERSE_Transaction) {
    return Object.assign({}, state, {
      transactions: state.transactions.concat(Object.assign({ transaction_id: ++id }, action.payload))
    });
  }
  return state;
};

export default TransactionReducer;
