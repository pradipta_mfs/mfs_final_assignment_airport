import { ADD_AIRCRAFT, DELETE } from "../Constants/actionTypes";
let id = 2;

const initialState = {
  aircrafts: [{
    aircraft_id: 1,
    aircraft_no: '6E 292',
    airline: 'Indigo'
  }, {
    aircraft_id: 2,
    aircraft_no: '6E 9438',
    airline: 'Indigo'
  }]
};

function AircraftReducer(state = initialState, action) {
  if (action.type === ADD_AIRCRAFT) {
    return Object.assign({}, state, {
      aircrafts: state.aircrafts.concat(Object.assign({ aircraft_id: ++id }, action.payload))
    });
  } else if(action.type === DELETE) {
    return Object.assign({}, state, {
      aircrafts: state.aircrafts.filter(el => el.aircraft_id !== action.payload)
    });
  }
  return state;
};

export default AircraftReducer;
