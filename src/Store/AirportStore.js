import { createStore } from "redux";
import AirportReducer from "../Reducers/AirportReducer";

const AirportStore = createStore(AirportReducer);

export default AirportStore;
