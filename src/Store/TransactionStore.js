import { createStore } from "redux";
import TransactionReducer from "../Reducers/TransactionReducer";

const TransactionStore = createStore(TransactionReducer);

export default TransactionStore;

