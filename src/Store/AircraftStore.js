import { createStore } from "redux";
import AircraftReducer from "../Reducers/AircraftReducer";

const AircraftStore = createStore(AircraftReducer);

export default AircraftStore;
