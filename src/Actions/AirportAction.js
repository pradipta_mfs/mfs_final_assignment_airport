import { ADD_AIRPORT, CALCULATE_Transaction, DELETE } from "../Constants/actionTypes";

export function addAirport(payload) {
  return { type: ADD_AIRPORT, payload }
};

export function calculateAvailableFuel(payload) {
  return { type: CALCULATE_Transaction, payload }
};

export function deleteAirport(payload) {
  return { type: DELETE, payload }
};
