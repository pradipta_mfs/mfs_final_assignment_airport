import { ADD_AIRCRAFT, DELETE } from "../Constants/actionTypes";

export function addAircraft(payload) {
  return { type: ADD_AIRCRAFT, payload }
};

export function deleteAircraft(payload) {
  return { type: DELETE, payload }
};
