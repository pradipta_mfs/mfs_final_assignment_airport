import { ADD_Transaction, REVERSE_Transaction, DELETE } from "../Constants/actionTypes";

export function addTransaction(payload) {
  return { type: ADD_Transaction, payload }
};

export function reverseTransaction(payload) {
  return { type: REVERSE_Transaction, payload }
};

export function deleteTransaction(payload) {
  return { type: DELETE, payload }
};
