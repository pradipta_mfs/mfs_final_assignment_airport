import React, { useState, useEffect } from "react";
import { Nav, Navbar, Tab, Row, Col } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import "./App.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import Routes from "./Routes";
import PrivateRoutes from "./PrivateRoutes";
import { AppContext } from "./libs/contextLib";

function App() {
  const [isAuthenticated, userHasAuthenticated] = useState(false);
  const [isAuthenticating, setIsAuthenticating] = useState(true);

  useEffect(() => {
    onLoad();
  }, []);

  async function onLoad() {
    if(localStorage.getItem('user')) {
      userHasAuthenticated(true);
    }

    setIsAuthenticating(false);
  }

  function handleLogout() {
    localStorage.removeItem('user');
    userHasAuthenticated(false);
  }

  return (
    !isAuthenticating &&
    <div>
      <Navbar collapseOnSelect bg="light" expand="lg">
        <Navbar.Brand>
          {
            isAuthenticated &&
            <LinkContainer to="/">
              <Nav.Link>Home</Nav.Link>
            </LinkContainer>
          }
        </Navbar.Brand>
        <Navbar.Toggle/>
        <Navbar.Collapse className="justify-content-end" >
          <Nav>
            {isAuthenticated
              ? <Nav.Link onClick={handleLogout}>Logout</Nav.Link>
              :
              <>
                <LinkContainer to="/login">
                  <Nav.Link>Login</Nav.Link>
                </LinkContainer>
              </>
            }
          </Nav>
        </Navbar.Collapse>
      </Navbar>
      <Tab.Container id="left-tabs-example">
        <Row>
          <Col sm={2} style={{'padding': '80px 20px'}}>
            {isAuthenticated &&
              <Nav variant="tabs" className="flex-column">
                <LinkContainer to="/Airports">
                  <Nav.Link>Airports</Nav.Link>
                </LinkContainer>
                <LinkContainer to="/Aircrafts">
                  <Nav.Link>Aircrafts</Nav.Link>
                </LinkContainer>
                <LinkContainer to="/Transactions">
                  <Nav.Link>Transactions</Nav.Link>
                </LinkContainer>
                <LinkContainer to="/Reports">
                  <Nav.Link>Reports</Nav.Link>
                </LinkContainer>
              </Nav>
            }
          </Col>
          <Col sm={9}>
            <Tab.Content>
              <AppContext.Provider value={{ isAuthenticated, userHasAuthenticated }}>
                <Routes />
                <PrivateRoutes />
              </AppContext.Provider>
            </Tab.Content>
          </Col>
        </Row>
      </Tab.Container>
    </div>
  );
}

export default App;
