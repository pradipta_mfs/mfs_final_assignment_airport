import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import Home from "./Containers/Home";
import Airports from "./Containers/Airports";
import Aircrafts from "./Containers/Aircrafts";
import Reports from "./Containers/Reports";
import Transactions from "./Containers/Transactions";
import { useAppContext } from "./libs/contextLib";
import AirportStore from "./Store/AirportStore"
import AircraftStore from "./Store/AircraftStore"
import TransactionStore from "./Store/TransactionStore"
import { Provider } from "react-redux"
import { ToastProvider } from 'react-toast-notifications'

function PrivateRoutes() {
  const { isAuthenticated } = useAppContext();

  return (
    isAuthenticated ?
    <Switch>
      <Route exact path="/">
        <Home />
      </Route>
      <Route exact path="/Airports">
        <Provider store = {AirportStore}>
          <ToastProvider>
            <Airports />
          </ToastProvider>
        </Provider>
      </Route>
      <Route exact path="/Aircrafts">
        <Provider store = {AircraftStore}>
          <ToastProvider>
            <Aircrafts />
          </ToastProvider>
        </Provider>
      </Route>
      <Route exact path="/Transactions">
        <Provider store = {TransactionStore}>
          <ToastProvider>
            <Transactions />
          </ToastProvider>
        </Provider>
      </Route>
      <Route exact path="/Reports">
        <Reports />
      </Route>
    </Switch> :
    <Redirect to="/Login" />
  );
}

export default PrivateRoutes;
