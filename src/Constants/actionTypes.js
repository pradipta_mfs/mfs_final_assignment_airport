const ADD_AIRPORT = "ADD_AIRPORT";
const ADD_AIRCRAFT = "ADD_AIRCRAFT";
const ADD_Transaction = "ADD_Transaction";
const REVERSE_Transaction = "REVERSE_Transaction";
const CALCULATE_Transaction = "CALCULATE_Transaction";
const DELETE = "DELETE";

export { ADD_AIRPORT, ADD_AIRCRAFT, ADD_Transaction, REVERSE_Transaction, CALCULATE_Transaction, DELETE };
